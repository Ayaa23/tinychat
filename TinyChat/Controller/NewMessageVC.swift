//
//  NewMessageVC.swift
//  TinyChat
//
//  Created by aya reda on 10/27/17.
//  Copyright © 2017 aya reda. All rights reserved.
//

import UIKit
import Firebase

class NewMessageVC: UIViewController {

    @IBOutlet weak var UserEmail: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var send: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        textView.delegate = self
        send.bindTOKeyboard()

    }

  
    @IBAction func sendBtn(_ sender: Any) {
        if textView.text != nil && textView.text != "Type a message ....."{
           send.isEnabled = false
            DataService.instance.uplodMSG(withMessage: textView.text, forUID: (Auth.auth().currentUser?.uid)!, forGroupKey: nil, sendComplete: { (isComplete) in
                if isComplete {
                    self.send.isEnabled = true
                }else{
                     self.send.isEnabled = true
                    print("An error!!")
                }
            })
        }
    }
    
    @IBAction func closeBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    }
extension NewMessageVC:UITextViewDelegate{
    func textViewDidBeginEditig(_ textView : UITextView){
        textView.text = " "
    }
}


