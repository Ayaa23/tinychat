//
//  ChatVC.swift
//  TinyChat
//
//  Created by aya reda on 10/27/17.
//  Copyright © 2017 aya reda. All rights reserved.
//

import UIKit
import Firebase

class ChatVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var messageArray = [Message]()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self

    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DataService.instance.getAllChatMsgs { (returnMessagesArray) in
            self.messageArray = returnMessagesArray.reversed()
            self.tableView.reloadData()
        }
    }

    @IBAction func newMessageBtn(_ sender: Any) {
        let  NewMessageVC  = storyboard?.instantiateViewController(withIdentifier: "NewMessageVC")
        
        present(NewMessageVC!, animated: true, completion: nil)
        
    }
    

}
extension ChatVC : UITableViewDelegate , UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messageArray.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ChatCell") as? ChatCell else {
            return UITableViewCell()
        }
        let message = messageArray[indexPath.row]
        
        DataService.instance.getUserName(forUID: message.senderId) { (returnedUserName) in
             cell.configureCell(email: returnedUserName , content: message.content)
        }
       
        return cell
    }
    
}











