//
//  LoginVC.swift
//  TinyChat
//
//  Created by aya reda on 10/25/17.
//  Copyright © 2017 aya reda. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

    @IBOutlet weak var Passwordtxt: UITextField!
    @IBOutlet weak var Emailtxt: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
         Emailtxt.delegate = self
        Passwordtxt.delegate = self
    }
    
    
    @IBAction func LoginBtn(_ sender: Any) {
        if Emailtxt.text != nil && Passwordtxt.text != nil {
            AuthService.instance.loginUser(withEmail: Emailtxt.text!, withPassword: Passwordtxt.text!, LoginComplete: { (success, loginError) in
                if success {
                   // self.dismiss(animated: true, completion: nil)
                    let  ChatVC  = self.storyboard?.instantiateViewController(withIdentifier: "ChatVC")
                    
                self.present(ChatVC!, animated: true, completion: nil)
                    
                    
                }
                else{
                    print( String( describing : loginError?.localizedDescription))
                }
                AuthService.instance.registerUser(withEmail: self.Emailtxt.text!, withPassword: self.Passwordtxt.text!, userCreationComplete: { (success, registreationError) in
                    if success {
                        AuthService.instance.loginUser(withEmail: self.Emailtxt.text! , withPassword: self.Passwordtxt.text! , LoginComplete: { (success, nil) in
                            // self.dismiss(animated: true, completion: nil)
                            let  ChatVC  = self.storyboard?.instantiateViewController(withIdentifier: "ChatVC")
                            
                            self.present(ChatVC!, animated: true, completion: nil)
                            
                           print("Successfully registered user!")
                            })
                        
                    } else{
                         print( String( describing : registreationError?.localizedDescription))
                        
                        
                    }
                })
            
            
            
            })
            
        }
        
        
        
        
    }
    
    @IBAction func CloseBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        
    }
    
}
extension LoginVC:UITextFieldDelegate{}
