//
//  UIViewExt.swift
//  TinyChat
//
//  Created by aya reda on 10/27/17.
//  Copyright © 2017 aya reda. All rights reserved.
//

import UIKit
extension UIView {
    func bindTOKeyboard (){
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(_:)) , name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        
        
        
    }
   @objc func keyboardWillChange (_ notification : NSNotification){
        let duration = notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! double_t
        let curve = notification.userInfo![UIKeyboardAnimationCurveUserInfoKey] as! UInt
        let beginningFrame = (notification.userInfo![UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let EndFrame = (notification.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let deltaY = EndFrame.origin.y - beginningFrame.origin.y
        UIView.animateKeyframes(withDuration: duration, delay: 0.0, options: UIViewKeyframeAnimationOptions(rawValue : curve), animations: {
            self.frame.origin.y += deltaY
        }, completion: nil)
        
    }
    
    
    
}
