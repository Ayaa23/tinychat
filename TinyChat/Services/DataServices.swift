//
//  DataServices.swift
//  TinyChat
//
//  Created by aya reda on 10/25/17.
//  Copyright © 2017 aya reda. All rights reserved.
//

import Foundation
import Firebase

let DB_BASE = Database.database().reference()

class DataService {
    static let instance = DataService()
    
    private var _REF_BASE = DB_BASE
    private var _REF_USERS = DB_BASE.child("users")
    private var _REF_CHAT = DB_BASE.child("chat")
    
    
    var REF_BASE : DatabaseReference{
        return _REF_BASE
    }
    
    var REF_USERS : DatabaseReference{
        return _REF_USERS
    }
    
    var REF_CHAT: DatabaseReference{
        return _REF_CHAT
        
    }
    
    
    func creatDBUser(uid: String,userData : Dictionary <String, Any>)  {
        REF_USERS.child(uid).updateChildValues(userData)
    }
    
    func uplodMSG(withMessage message : String , forUID uid : String , forGroupKey groupKey : String? , sendComplete :  @escaping (_ status : Bool)-> () )  {
        if groupKey != nil {
    
        }
        else {
            REF_CHAT.childByAutoId().updateChildValues(["content": message, "senderId" : uid])
            sendComplete(true)
        }
       
            }
    func getUserName(forUID uid :String , handler : @escaping (_ username : String)->()){
        REF_USERS.observeSingleEvent(of: .value) { (userSnapShot) in
            guard let userSnapShot = userSnapShot.children.allObjects as? [DataSnapshot] else {return}
            for user in userSnapShot{
                if user.key == uid {
                    handler(user.childSnapshot(forPath: "email").value as! String)
                    
                }
            }
        }
        
    }
    
    
    
    
   func getAllChatMsgs( handler : @escaping (_ messages : [Message])->()) {
    var messageArray = [Message]()
    
    REF_CHAT.observeSingleEvent(of: .value) { (ChatMessageSnapshot) in
        guard let ChatMessageSnapshot = ChatMessageSnapshot.children.allObjects as? [DataSnapshot] else{return}
        
        for message in ChatMessageSnapshot {
            let content = message.childSnapshot(forPath: "content") as! String
            let senderID = message.childSnapshot(forPath: "senderID") as! String
            let message = Message(content : content , senderID : senderID)
            messageArray.append(message)
            
            
            
        }
        
        handler(messageArray)
        
    }
    
}
}



