//
//  Message.swift
//  TinyChat
//
//  Created by aya reda on 10/28/17.
//  Copyright © 2017 aya reda. All rights reserved.
//

import Foundation
class Message {
    private var _content : String
    private var _senderID: String
    
    var content : String {
        return _content
    }
    
    var senderId : String{
        return _senderID
        
    }
    init(content : String , senderID : String) {
        self._content = content
        self._senderID = senderID
    }
    
    
    
}

