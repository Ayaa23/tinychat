//
//  ChatCell.swift
//  TinyChat
//
//  Created by aya reda on 10/28/17.
//  Copyright © 2017 aya reda. All rights reserved.
//

import UIKit

class ChatCell: UITableViewCell {

    @IBOutlet weak var emailLBL: UILabel!
    
    @IBOutlet weak var MsgContentLBL: UILabel!
    
    func configureCell (  email : String , content : String){
        self.emailLBL.text = email
        self.MsgContentLBL.text = content
    }
}
